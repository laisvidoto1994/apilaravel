<?php 

namespace App\Http\Controllers; 

use App\Address; 
use App\Services\Service;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Classe Controlador de Enderreço
 */
class AddressController extends Controller 
{

    protected $service;

    public function __construct(Service $service)
    {
        $this->service  = $service;
    }

    /**
     * Metodo de busca de endereço pelo CEP
     */
    public function searchAddress(Request $request)
    {
        return $this->service->searchAddresses($request);
    }

    /**
     * Metodo de Cadastro de endereço pelo CEP
     */
    public function createAddress(Request $request)
    {
        return $this->service->createAddresses($request);
    }

    /**
     * Metodo de Cadastro de endereço pelo CEP
     */
    public function deleteAddress(Request $request)
    {
        return $this->service->deleteAddress($request);
    }


}