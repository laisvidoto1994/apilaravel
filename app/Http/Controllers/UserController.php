<?php 

namespace App\Http\Controllers; 

use App\User; 
use App\Services\Service;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller 
{
    protected $service;

    public function __construct(Service $service)
    {
        $this->service  = $service;
    }

    public function showUser($id)
    { 
        return User::findOrFail($id); 
    }


    public function cadastroUser(Request $request)
    {
        return $this->service->cadastroUser($request);
    }


    public function updateUser(Request $request)
    {
        return $this->service->updateUser($request);
    }


    public function deleteUser(Request $request)
    {
        return $this->service->deleteUser($request);
    }


    public function listAllUser(Request $request)
    {
        return $this->service->listAllUser($request);
    }

}