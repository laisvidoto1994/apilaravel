<?php

namespace App\Services;

use App\Address;
use GuzzleHttp\Client;
use App\Http\Controllers\AddressController;

/**
* Class Service
* @package App\Services
*/
class Service
{
    /**
     * Metodo de Busca de Endereço pelo CEP
     */
    public static function searchAddresses($cep, $response = false)
    {
        try
        {
            $cep = preg_replace('/[.-\/]+/','', $cep);
            $url = 'https://viacep.com.br/ws/'.$cep.'/json/';

            $client = new Client();
            $res = $client->get($url);

            $data = json_decode( $res->getBody() );
        
            if( empty($data) )
            {
                if ($response == false) 
                {
                    throw new \Exception('Invalid response');
                }
                else
                {
                    throw new \Exception('CEP Invalido');
                }
            }
            
            return 
            [
                'zip_code'    => $cep,
                'street'      => $data->logradouro,
                'district'    => $data->bairro,
                'city'        => $data->localidade,
                'uf'          => $data->uf,
                'street_view' => 'maps.google.co.in/maps?q='.$cep
            ];
        }
        catch  (\Exception $e)
        {
            return response()->json( ['error' => true, 'message' => $e->getMessage()], 500 );
        }
    }


    /**
     * Metodo de Busca de Endereço pelo CEP
     */

    public static function createAddresses($cep)
    {
        try
        {
            $ceps = preg_replace('/-/','', $cep,1);
            \Log::debug($ceps);
            $url = 'https://viacep.com.br/ws/'.$ceps.'/json/';

            $client = new Client();
            $res = $client->get( $url );

            $data = json_decode( $res->getBody() );


            if( empty($data) )
            {
                throw new \Exception('CEP Invalido');
            }
            else
            {
                $data = [

                    "zip_code"      => $ceps,
                    'logradouro'    => $data->logradouro,
                    'neighborhood'  => $data->bairro,
                    'locality'      => $data->localidade,
                    'uf'            => $data->uf,
                    'street_view'   => 'maps.google.co.in/maps?q='.$cep
                ];

                $address = new Address($data);
                $address->save();

                if( empty($address) )
                {
                    throw new \Exception('CEP não preenchido');
                }
                return response()->json(['error' => false, 'message' => 'Endereco cadastrado com sucesso!'], 200);
            }
        }
        catch  (\Exception $e)
        {
            return response()->json( ['error' => true, 'message' => $e->getMessage()], 500 );
        }
    }

    /**
     * Metodo de Exclusão de Enderreço
     */
    public function deleteAddress(Request $request)
    {
        $data =
            [
                "zip_code"      => $request->cep,
                'logradouro'    => $request->logradouro,
                'neighborhood'  => $request->bairro,
                'locality'      => $request->localidade,
                'uf'            => $request->uf,
                'street_view'   => 'maps.google.co.in/maps?q='.$request->get('cep')
            ];

        try
        {
            $Address = Address::where('id', $request->get('id') )->delete();

            if( !$Address )
            {
                throw new \Exception('Enderreço inesistente, informe outro Enderreço!');
            }
            return response()->json(['error' => false,'message' => 'Enderreço Excluido com sucesso!'], 200);
        }
        catch (\Exception $e)
        {
            return response()->json(['error' => true, 'message' => $e->getMessage()], 401);
        }

    }

    /**
     * Metodo de Cadastro de um Usuario
     */
    public function cadastroUser(Request $request)
    {

        \Log::debug($request->all());

        $data =
            [
                "name"     => $request->get('name'),
                "email"    => $request->get('email'),
                "password" => bcrypt($request->get('password'))
            ];

        try
        {

            $user = User::save($data);

            if( empty($user) )
            {
                throw new \Exception('usuario vazio, não cadastrado');
            }
            return response()->json(['error' => false, 'message' => 'usuario criado com sucesso!'], 200);
        }
        catch (\Exception $e)
        {
            return response()->json(['error' => true, 'message' => $e->getMessage()], 401);
        }

    }

    /**
     * Metodo de Atualização de um Usuario
     */
    public function updateUser(Request $request)
    {
        $data =
            [
                "name"     => $request->get('name'),
                "email"    => $request->get('email'),
                "password" => bcrypt($request->get('password'))
            ];

        try
        {
            $user = User::where('id', $request->get('id') )->update($data);

            \Log::debug($user);

            if( !$user )
            {
                throw new \Exception('usuario vazio, não atualizado');
            }
            return response()->json(['error' => false,'message' => 'usuario atualizado com sucesso!'], 200);
        }
        catch (\Exception $e)
        {
            return response()->json(['error' => true, 'message' => $e->getMessage()], 401);
        }
    }

    /**
     * Metodo de Exclusão de um Usuario
     */
    public function deleteUser(Request $request)
    {
        $data =
            [
                "name"     => $request->get('name'),
                "email"    => $request->get('email'),
                "password" => bcrypt($request->get('password'))
            ];

        try
        {
            $user = User::where('id', $request->get('id') )->delete();

            if( !$user )
            {
                throw new \Exception('usuario inesistente, informe outro usuario!');
            }
            return response()->json(['error' => false,'message' => 'usuario excluido com sucesso!'], 200);
        }
        catch (\Exception $e)
        {
            return response()->json(['error' => true, 'message' => $e->getMessage()], 401);
        }
    }

    /**
     * Metodo de Listagem de todos os Usuario
     */
    public function listAllUser(Request $request)
    {
        try
        {
            $user = User::all();

            if( !$user )
            {
                throw new \Exception('não há usuarios!');
            }
            return $user;
            //return response()->json(['error' => false,'message' => 'Listagem de usuarios!'], 200);

        }
        catch (\Exception $e)
        {
            return response()->json(['error' => true, 'message' => $e->getMessage()], 401);
        }
    }



}