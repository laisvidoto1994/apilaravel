<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!|
*/

Route::middleware('auth:api')->group(function()
{
   Route::get('/user', function (Request $request)
    {
        return $request->user();
    });

    Route::prefix('user')->group(function ()
    {
         Route::get('/{id}', 'UserController@showUser');
         Route::post('/create', 'UserController@cadastroUser');
         Route::post('/update', 'UserController@updateUser');
         Route::post('/delete', 'UserController@deleteUser');
         Route::post('/lista', 'UserController@listAllUser');
    });

    Route::prefix('address')->group(function ()
    {
        Route::post('/search', 'AddressController@searchAddress');
        Route::post('/create', 'AddressController@createAddress');
        Route::post('/delete', 'AddressController@createAddress');
    });

});